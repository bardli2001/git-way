# hand on git

Date Created: November 4, 2021 4:44 PM
Status: Next Up

## Install your git

https://git-scm.com/book/en/v2/Getting-Started-Installing-Git

## Get hand on ESSENCE

First, we need a space for our files. You may want create a new folder for this project.

Second, find your folder with Command Prompt

1. open Command Prompt (CMD)
2. You would see some thing like `C:\WINDOWS\system32>`

![CMDpic](./Pic/CMD_pic.png?raw=true)

It shows we are in disc C, and Folder system32

1. use `cd` to switch your location on your computer. Format: `cd <File Path>`
   
    > Example: cd E:\admin\Study\Programming\ESSENCE space\ESSENCEgit
    
    Hint: Use `cd E:` to switch between your disk

Third, download all the files from Gitlab

`git clone https://gitlab.com/essence.yorku/essence-onboard-software.git`

Now we are on the end of master now. The file on server is same as our computer.

![Master](./Pic/master.png?raw=true)

## Create your branch

Before we start, we should know **which branch we are**(the location of our head), because all our git command will operate base on where we are.

> We could find this information on right bottom on your Visual Studio Code

![Master](./Pic/find_branch.png?raw=true)

This command **create new branch**, and move you on that branch.

`git checkout -b <branch name>`

> Example: git checkout -b lunch

> (This will create a branch name: lunch ,on master)

![branch](./Pic/create_branch.png?raw=true)

We could **switch between branch** and your branch
`git checkout <branch name>`

> Example: git checkout master

![branchswich](./Pic/switch_branch.png?raw=true)

## update your code to server

**Know where we are.** We should update our code on our branch(Head on your branch)

![create_branch](./Pic/create_branch.png?raw=true)

Write some code and ready to update.

### **2 steps to update**

1. `git commit -am <commit message>`
   
    > Example: git commit -am "fix some errors"
2. `git push`
   
    > git push

![push](./Pic/push.png?raw=true)

